from bson.json_util import dumps
from bson.objectid import ObjectId	

def getAllPersons(db):
    persons=db.persons
    return dumps(list(persons.find()))


def getPerson(db, id):
    persons=db.persons
    print("Consulting person by ID '"+id+"'")
    return dumps(persons.find_one({'_id':ObjectId(id)}))

def getAllCodifications(db):
    codifications=db.codifications
    return dumps(list(codifications.find()))


def getACodification_all(db):
    #TODO modificar para que solo devuelva uno por cada persona
    codifications=db.codifications
    return dumps(list(codifications.find()))


def getPersonCodifications(db, id):
    codifications=db.codifications
    return dumps(list(codifications.find({'person_id':ObjectId(id)})))


def addPerson(db, person):
    persons=db.persons
    person_data={
        'name': person['name'],
	'level': person['level']
    }
    return str(persons.insert_one(person_data).inserted_id)

def editPerson (db, person):
    persons=db.persons
    person_id=ObjectId(person['person_id'])
    person_data={
        'name': person['name'],
	'level': person['level']
    }
    return str(persons.update_one({'_id':person_id}, {"$set": person_data}, upsert=False))

def addCodification(db, data):
    codifications=db.codifications
    codification_data={
        'person_id': ObjectId(data['person_id']),
        'codification': data['codification']
    }
    return str(codifications.insert_one(codification_data).inserted_id)


def deletePerson(db, id):
    codifications=db.codifications
    persons=db.persons
    codifications.delete_many({'person_id': ObjectId(id)})
    persons.delete_one({'_id': ObjectId(id)})
