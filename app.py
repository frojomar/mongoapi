from flask import Flask, Response, jsonify, request
from pymongo import MongoClient
import mongoDBController as mongo
import readConfigurationsFile as conf
from flask_cors import CORS
import json

configurations = {}
conf.loadConfigurations('settings.conf', configurations)
print(configurations)
client = MongoClient('mongodb://'+str(configurations['HOST_MONGO'])+':'+str(configurations['PORT_MONGO'])+'/')
db = client[str(configurations['DATABASE_MONGO'])]



app = Flask(__name__)
CORS(app)

@app.route('/hello', methods=['GET'])
def test():
    return 'Hello!'


@app.route('/persons', methods=['GET'])
def get_persons():
    return mongo.getAllPersons(db)
    #return 'Getting all persons!'


@app.route('/persons/<id>', methods=['GET'])
def get_person(id):
    return mongo.getPerson(db, id)
    #return 'Getting a person! [ID: '+str(id)+']'


@app.route('/persons/codifications', methods=['GET'])
def get_codifications():
    return mongo.getAllCodifications(db)
    #return 'Getting all codifications!'


@app.route('/persons/onecodification', methods=['GET'])
def get_one_codification():
    return mongo.getACodification_all(db)
    #return 'Getting a codification per person!'


@app.route('/persons/<id>/codifications', methods=['GET'])
def get_person_codifications(id):
    return mongo.getPersonCodifications(db, id)
    #return 'Getting all codifications of a person![ID: '+str(id)+']'


@app.route('/persons', methods=['POST'])
def add_person():
    person=json.loads(request.data)
    print('Adding a person!')
    print(person)
    return jsonify({'insertedId':mongo.addPerson(db, person)})


@app.route('/persons/<id>', methods=['PUT'])
def edit_person(id):
    print('Editing a person!')
    person=json.loads(request.data)
    print(person)
    if id!= person['person_id']:
        return jsonify({'error':'id of path must be the same of person_id'}), 400
    else:
        return jsonify({'insertedId':mongo.editPerson(db, person)}), 200



@app.route('/persons/<id>/codifications', methods=['POST'])
def add_codification(id):
    data=json.loads(request.data)
    print('Adding a codification to a person![ID: '+str(id)+']')
    print(data)
    return jsonify({'insertedId':mongo.addCodification(db, data)})


@app.route('/persons/<id>', methods=['DELETE'])
def delete_person(id):
    mongo.deletePerson(db, id)
    print('Deleting a person![ID: '+str(id)+']')
    return ('', 204)


if __name__ == '__main__':
    app.run(host=str(configurations['HOST']), port=str(configurations['PORT']))
