#!/bin/bash
host=qapps.unex.es
port=7777
export FLASK_APP=./main.py
source venv/bin/activate
flask run -h $host -p $port
